package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/glossd/commandserver/conf"
	"gitlab.com/glossd/commandserver/db"
	"gitlab.com/glossd/commandserver/web/router"
)

// @title Command Server API
// @schemes https
// @license.name TheCompany
// @host thecompany.com
// @BasePath /api/commandserver
func main() {
	ctx, dbClient, err := db.Init()
	if err != nil {
		log.Fatal(err)
	}
	defer dbClient.Disconnect(ctx)

	log.Infof("Starting server on %d", conf.Props.Port)
	log.Fatal(router.New().Run(fmt.Sprintf(":%d", conf.Props.Port)))
}
