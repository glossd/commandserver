package domain

import (
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type JobStatus string
const (
	Running JobStatus = "running"
	Completed JobStatus = "completed"
	Failed JobStatus = "failed"
)

type JobType string
const (
	Counter JobType = "counter"
)

type Job struct {
	ID primitive.ObjectID `bson:"_id"`
	Type JobType
	Status JobStatus

	N int
	Delay time.Duration

	CreatedAt int64
	UpdatedAt int64
}

func NewCounterJob(n int, delay time.Duration) (Job, error) {
	if !(n >= 1 &&  n <= 1000) {
		return Job{}, fmt.Errorf("n must be 1 <= n <= 1000")
	}
	now := time.Now().Unix()
	return Job{
		ID:        primitive.NewObjectID(),
		Type:      Counter,
		Status:    Running,
		N:         n,
		Delay:     delay,
		CreatedAt: now,
		UpdatedAt: now,
	}, nil
}

func (j Job) GetID() string {
	return j.ID.String()
}

func (j Job) Complete() {
	j.Status = Completed
	j.UpdatedAt = time.Now().Unix()
}

func (j Job) Fail() {
	j.Status = Failed
	j.UpdatedAt = time.Now().Unix()
}
