package mqpub

import (
	"github.com/confluentinc/confluent-kafka-go/kafka"
	log "github.com/sirupsen/logrus"
	"gitlab.com/glossd/commandserver/conf"
)

// https://github.com/confluentinc/confluent-kafka-go#examples

type Publisher struct {
	producer *kafka.Producer
	topic string
}

func InitPublisher(topic string) (Publisher, error) {
	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": conf.Props.Kafka.Host})
	if err != nil {
		log.Errorf("Failed to init kafka producer: %v", err)
		return Publisher{}, err
	}
	return Publisher{producer: p, topic: topic}, nil
}

func (p Publisher) Publish(msg string) error {
	err := p.producer.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &p.topic, Partition: kafka.PartitionAny},
		Value:          []byte(msg),
	}, nil)

	// Wait for message deliveries before shutting down
	p.producer.Flush(int(conf.Props.Kafka.DeliveryWaitingTime))
	return err
}