package mqsub

import (
	"context"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	log "github.com/sirupsen/logrus"
	"gitlab.com/glossd/commandserver/conf"
)

// https://github.com/confluentinc/confluent-kafka-go#examples

func Subscribe(ctx context.Context, topic string, handler func(string)) error {
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": conf.Props.Kafka.Host,
		//"group.id":          "myGroup",
		"auto.offset.reset": "earliest",
	})

	if err != nil {
		return err
	}

	err = c.Subscribe(topic, nil)
	if err != nil {
		return err
	}

	for {
		select {
		case <-ctx.Done():
			c.Close()
			return ctx.Err()
		default:
			msg, err := c.ReadMessage(-1)
			if err == nil {
				handler(string(msg.Value))
			} else {
				// The client will automatically try to recover from all errors.
				log.Errorf("Consumer error: %v (%v)\n", err, msg)
			}
		}
	}
}

func ReadAll(ctx context.Context, topic string) ([]string, error) {
	var result []string
	err := Subscribe(ctx, topic, func(s string) {
		result = append(result, s)
	})
	if err != nil {
		return nil, err
	}
	return result, nil
}
