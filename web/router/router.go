package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/glossd/commandserver/web/rest"
	"gitlab.com/glossd/commandserver/web/ws"
)

const BasePath = "/api/commandserver"

func New() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(gin.Recovery())

	base := r.Group(BasePath)

	base.POST("/jobs", rest.PostJob)
	base.GET("/jobs", rest.GetJobs)
	base.GET("/jobs/:id", rest.GetJob)
	base.GET("/jobs/:id", rest.GetJob)
	base.GET("/jobs/:id/logs", rest.GetJobLogs)

	base.GET("/ws/jobs/:id/logs", ws.WsStreamLogs)
	return r
}
