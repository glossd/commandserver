package rest

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/glossd/commandserver/cmdcenter"
	"gitlab.com/glossd/commandserver/db"
	"gitlab.com/glossd/commandserver/domain"
	"net/http"
	"time"
)

type Job struct {
	ID string `json:"id"`
	Type string `json:"type"`
	Status string `json:"status"`
	CreatedAt int64 `json:"createdAt"`
}

func toJob(j domain.Job) Job {
	return Job{
		ID:     j.GetID(),
		Type: string(j.Type),
		Status: string(j.Status),
		CreatedAt: j.CreatedAt,
	}
}

type PostJobRequest struct {
	N int `json:"n"`
	Delay time.Duration `json:"delay"`
}

// @ID post job
// @Success 200 {object} Job
// @Failure 400 {object} ErrorRes
// @Failure 500 {object} ErrorRes
// @Router /jobs [post]
func PostJob(c *gin.Context) {
	var input PostJobRequest
	err := c.BindJSON(&input)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, E(err))
		return
	}
	j, err := cmdcenter.StartCounterJob(c.Request.Context(), input.N, input.Delay)
	if err != nil {
		handleError(c, err)
		return
	}

	c.JSON(http.StatusCreated, toJob(j))
}

// @ID get jobs
// @Success 200 {array} Job
// @Failure 400 {object} ErrorRes
// @Failure 500 {object} ErrorRes
// @Router /jobs [get]
func GetJobs(c *gin.Context) {
	// todo pagination
	jobs, err := db.FindJobs(c.Request.Context())
	if err != nil {
		handleError(c, err)
		return
	}
	result := make([]Job, 0, len(jobs))
	for _, job := range jobs {
		result = append(result, toJob(job))
	}
	c.JSON(http.StatusOK, result)
}

// @ID get job
// @Param id path string true "Job ID"
// @Success 200 {object} Job
// @Failure 400 {object} ErrorRes
// @Failure 500 {object} ErrorRes
// @Router /jobs/{id} [get]
func GetJob(c *gin.Context) {
	job, err := db.FindJob(c.Request.Context(), c.Param("id"))
	if err != nil {
		handleError(c, err)
		return
	}
	c.JSON(http.StatusOK, toJob(job))
}

// @ID get job logs
// @Param id path string true "Job ID"
// @Success 200 {array} string
// @Failure 400 {object} ErrorRes
// @Failure 500 {object} ErrorRes
// @Router /jobs/{id}/logs [get]
func GetJobLogs(c *gin.Context) {
	logs, err := cmdcenter.GetJobLogs(c.Request.Context(), c.Param("id"))
	if err != nil {
		handleError(c, err)
		return
	}
	c.JSON(http.StatusOK, logs)
}

