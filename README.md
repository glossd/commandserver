# Command Server

![alt api](open-api.png "API") 

All the command outputs are stored in kafka (look in `cmdcenter/run.go`).  
`web/mq` folder contains kafka consumers and producers.  
Each job has its own kafka topic. I used mongodb for storing the state of jobs.  
I also added a websocket endpoint for streaming logs (look in `web/ws`).  
