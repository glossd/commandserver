package db

import (
	"context"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/glossd/commandserver/conf"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

var ErrVersionNotMatch = errors.New("version doesn't match")

var Client *mongo.Client

func Init() (context.Context, *mongo.Client, error) {
	return InitWithURI(GetURI())
}

func InitWithURI(uri string) (context.Context, *mongo.Client, error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	var err error
	Client, err = mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		return ctx, nil, err
	}
	log.Infof("Connected to MongoDB on %s", conf.Props.DB.Host)

	return ctx, Client, nil
}

func GetURI() string {
	dbConf := conf.Props.DB
	fullHost := dbConf.Host
	if dbConf.Port != nil {
		fullHost = fmt.Sprintf("%s:%d", fullHost, *dbConf.Port)
	}

	var creds string
	if dbConf.Username != "" {
		creds = fmt.Sprintf("%s:%s", dbConf.Username, dbConf.Password)
	}

	return fmt.Sprintf("%s://%s@%s/%s?retryWrites=true&w=majority",
		dbConf.Scheme,
		creds,
		fullHost,
		dbConf.Name)
}

func filterID(id interface{}) bson.M {
	return bson.M{"_id": id}
}

