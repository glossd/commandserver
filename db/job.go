package db

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/glossd/commandserver/conf"
	"gitlab.com/glossd/commandserver/domain"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

func InsertJob(ctx context.Context, job domain.Job) error {
	_, err := JobCol().InsertOne(ctx, job)
	if err != nil {
		log.Errorf("Failed to insert job %v : %v", job, err)
	}
	return err
}

func FindJob(ctx context.Context, id string) (domain.Job, error){
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return domain.Job{}, err
	}
	var result domain.Job
	err = JobCol().FindOne(ctx, filterID(oid)).Decode(&result)
	return result, err
}

func UpdateJob(j domain.Job) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err := JobCol().ReplaceOne(ctx, filterID(j.ID), j)
	if err != nil {
		log.Errorf("Failed to update job %v : %v", j, err)
	}
	return err
}


func FindJobs(ctx context.Context) ([]domain.Job, error) {
	// A slice of docs for storing the decoded documents
	var docs []domain.Job

	cur, err := JobCol().Find(ctx, bson.D{})
	if err != nil {
		return docs, err
	}

	for cur.Next(ctx) {
		var job domain.Job
		err := cur.Decode(&job)
		if err != nil {
			return docs, err
		}
		docs = append(docs, job)
	}

	if err := cur.Err(); err != nil {
		return docs, err
	}

	// once exhausted, close the cursor
	cur.Close(ctx)

	if len(docs) == 0 {
		return []domain.Job{}, nil
	}

	return docs, nil
}

func JobCol() *mongo.Collection {
	return Client.Database(conf.Props.DB.Name).Collection("jobs")
}
