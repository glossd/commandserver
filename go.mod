module gitlab.com/glossd/commandserver

go 1.16

require (
	github.com/confluentinc/confluent-kafka-go v1.7.0
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.3.0 // indirect
	github.com/gorilla/websocket v1.4.2
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.9.0
	github.com/tidwall/pretty v1.0.2 // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.4.0
)
