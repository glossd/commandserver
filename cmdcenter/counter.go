package cmdcenter

import (
	"context"
	"gitlab.com/glossd/commandserver/db"
	"gitlab.com/glossd/commandserver/domain"
	"time"
)

func StartCounterJob(ctx context.Context, n int, delay time.Duration) (domain.Job, error) {
	job, err := domain.NewCounterJob(n, delay)
	if err != nil {
		return domain.Job{}, err
	}

	err = db.InsertJob(ctx, job)
	if err != nil {
		return domain.Job{}, err
	}

	err = run(job)
	if err != nil {
		return domain.Job{}, err
	}

	return job, nil
}



