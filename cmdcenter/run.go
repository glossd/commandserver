package cmdcenter

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/glossd/commandserver/db"
	"gitlab.com/glossd/commandserver/domain"
	"gitlab.com/glossd/commandserver/web/mq/mqpub"
	"time"
)

func run(j domain.Job) error {
	publisher, err := mqpub.InitPublisher(j.GetID())
	if err != nil {
		return err
	}
	go func() {
		switch j.Type {
		case domain.Counter:
			for i := 0; i < j.N; i++ {
				err := publisher.Publish(fmt.Sprintf("%d", i))
				if err != nil {
					log.Errorf("Failed to publish message: %v", err)
				}
				if i < j.N-1 {
					time.Sleep(j.Delay)
				}
			}
			j.Complete()
			// todo handle error
			_ = db.UpdateJob(j)
		}
	}()
	return nil
}


