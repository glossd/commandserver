package cmdcenter

import (
	"context"
	"gitlab.com/glossd/commandserver/db"
	"gitlab.com/glossd/commandserver/web/mq/mqsub"
)

func StreamJobLogs(ctx context.Context, jobID string, handler func(string)) error {
	job, err := db.FindJob(ctx, jobID)
	if err != nil {
		return err
	}
	return mqsub.Subscribe(ctx, job.GetID(), handler)
}

func GetJobLogs(ctx context.Context, jobID string) ([]string, error)  {
	job, err := db.FindJob(ctx, jobID)
	if err != nil {
		return nil, err
	}
	return mqsub.ReadAll(ctx, job.GetID())
}
