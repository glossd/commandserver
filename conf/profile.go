package conf

import "os"

var Profile = os.Getenv("PB_PROFILE")
var IsE2EVar = Profile == "e2e"
var IsLocalVar = Profile == "local"

func IsProd() bool {
	return !IsLocal()
}

func IsLocal() bool {
	return IsE2EVar || IsLocalVar
}

func IsE2E() bool {
	return IsE2EVar
}