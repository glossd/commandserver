package conf

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

type AppProps struct {
	Port int
	Stdout
	DB
	Kafka
}

type Stdout struct {
	Level string
}

type Kafka struct {
	Host                string
	DeliveryWaitingTime time.Duration `mapstructure:"delivery_waiting_time"`
}

type DB struct {
	Scheme   string
	Host     string
	Port     *int
	Name     string
	Username string
	Password string
}

var Props AppProps
var ProjectRoot = getRootProjectDir()

func init() {
	Init()
}

func Init() {
	ParseConfig(ProjectRoot, &Props)
	log.Debugf("Configuration properties: %+v", Props)
}

func ParseConfig(projectRoot string, props interface{}) {
	viper.SetConfigName("config")
	viper.AddConfigPath(projectRoot)
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}

	loadEnvVariables()

	err := viper.Unmarshal(props)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}
}

func loadEnvVariables() {
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.SetEnvPrefix("CS") // IMPORTANT!!!
	viper.AutomaticEnv()
}

func getRootProjectDir() string {
	_, b, _, _ := runtime.Caller(0)
	return filepath.Dir(filepath.Dir(b))
}
